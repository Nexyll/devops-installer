﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Timers;
using System.Windows;
using DevOpsInstaller.Annotations;
using Timer = System.Timers.Timer;

namespace DevOpsInstaller
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private double _progressBarValue = 0;
        private Random _random = new Random();
        private Timer _sTask;

        public string[] procode =
        {
            "static void unroll_tree_refs(struct audit_context *ctx,",
            "		      struct audit_tree_refs *p, int count)",
            "{",
            "	struct audit_tree_refs *q;",
            "	int n;",
            "	if (!p) {",
            "		/* we started with empty chain */",
            "		p = ctx->first_trees;",
            "		count = 31;",
            "		/* if the very first allocation has failed, nothing to do */",
            "		if (!p)",
            "			return;",
            "	}",
            "	n = count;",
            "	for (q = p; q != ctx->trees; q = q->next, n = 31) {",
            "		while (n--) {",
            "			audit_put_chunk(q->c[n]);",
            "			q->c[n] = NULL;",
            "		}",
            "	}",
            "	while (n-- > ctx->tree_count) {",
            "		audit_put_chunk(q->c[n]);",
            "		q->c[n] = NULL;",
            "	}",
            "	ctx->trees = p;",
            "	ctx->tree_count = count;",
            "}",
            "",
            "static void free_tree_refs(struct audit_context *ctx)",
            "{",
            "	struct audit_tree_refs *p, *q;",
            "	for (p = ctx->first_trees; p; p = q) {",
            "		q = p->next;",
            "		kfree(p);",
            "	}",
            "}",
            "static int match_tree_refs(struct audit_context *ctx, struct audit_tree *tree)",
            "{",
            "	struct audit_tree_refs *p;",
            "	int n;",
            "	if (!tree)",
            "		return 0;",
            "	/* full ones */",
            "	for (p = ctx->first_trees; p != ctx->trees; p = p->next) {",
            "		for (n = 0; n < 31; n++)",
            "			if (audit_tree_match(p->c[n], tree))",
            "				return 1;",
            "	}",
            "	/* partial */",
            "	if (p) {",
            "		for (n = ctx->tree_count; n < 31; n++)",
            "			if (audit_tree_match(p->c[n], tree))",
            "				return 1;",
            "	}",
            "	return 0;",
            "}",
            "static int audit_compare_uid(kuid_t uid,",
            "			     struct audit_names *name,",
            "			     struct audit_field *f,",
            "			     struct audit_context *ctx)",
            "{",
            "	struct audit_names *n;",
            "	int rc;",
            " ",
            "	if (name) {",
            "		rc = audit_uid_comparator(uid, f->op, name->uid);",
            "		if (rc)",
            "			return rc;",
            "	}",
            "	if (ctx) {",
            "		list_for_each_entry(n, &ctx->names_list, list) {",
            "			rc = audit_uid_comparator(uid, f->op, n->uid);",
            "			if (rc)",
            "				return rc;",
            "		}",
            "        static int put_tree_ref(struct audit_context *ctx, struct audit_chunk *chunk)",
            "{",
            "	struct audit_tree_refs *p = ctx->trees;",
            "	int left = ctx->tree_count;",
            "	if (likely(left)) {",
            "		p->c[--left] = chunk;",
            "		ctx->tree_count = left;",
            "		return 1;",
            "	}",
            "	if (!p)",
            "		return 0;",
            "	p = p->next;",
            "	if (p) {",
            "		p->c[30] = chunk;",
            "		ctx->trees = p;",
            "		ctx->tree_count = 30;",
            "		return 1;",
            "	}",
            "	return 0;",
            "}		",
            "static enum audit_state audit_filter_task(struct task_struct *tsk, char **key)",
            "{",
            "	struct audit_entry *e;",
            "	enum audit_state   state;",
            "",
            "	rcu_read_lock();",
            "	list_for_each_entry_rcu(e, &audit_filter_list[AUDIT_FILTER_TASK], list) {",
            "		if (audit_filter_rules(tsk, &e->rule, NULL, NULL,",
            "				       &state, true)) {",
            "			if (state == AUDIT_RECORD_CONTEXT)",
            "				*key = kstrdup(e->rule.filterkey, GFP_ATOMIC);",
            "			rcu_read_unlock();",
            "			return state;",
            "		}",
            "	}",
            "	rcu_read_unlock();",
            "	return AUDIT_BUILD_CONTEXT;",
            "}",
            "return audit_gid_comparator(cred->gid, f->op, cred->fsgid);",
            "	/* egid comparisons */",
            "	case AUDIT_COMPARE_EGID_TO_SGID:",
            "		return audit_gid_comparator(cred->egid, f->op, cred->sgid);",
            "	case AUDIT_COMPARE_EGID_TO_FSGID:",
            "		return audit_gid_comparator(cred->egid, f->op, cred->fsgid);",
            "	/* sgid comparison */",
            "	case AUDIT_COMPARE_SGID_TO_FSGID:",
            "		return audit_gid_comparator(cred->sgid, f->op, cred->fsgid);",
        };

        public double ProgressBarValue
        {
            get => _progressBarValue;
            set
            {
                if (value == _progressBarValue) return;
                _progressBarValue = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            ConsoleManager.Show();
            _sTask = new Timer(20);
            _sTask.AutoReset = true;
            _sTask.Elapsed += scheduledMethod;
            _sTask.Enabled = true;
        }

        private void scheduledMethod(object sender, ElapsedEventArgs e)
        {
            ProgressBarValue += 1;
            _sTask.Interval = _random.Next(4, 400);
            Console.WriteLine(procode[_random.Next(0, procode.Length - 1)]);
            if (ProgressBarValue >= 100)
            {
                _sTask.Enabled = false;
                Console.WriteLine("\nFÉLICITATION !\n\nLE DEVOPS A CORRECTEMENT ÉTÉ INSTALLÉ SUR LA MACHINE.");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}