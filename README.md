# DevOps Installer Pro

Petit gratuiciel pour installer le devops sur votre machine. Très utile dans le cadre de développements professionnels et à forts enjeux de qualité et d'agilité.

N'oubliez pas de lancer docker avant de démarrer le processus d'installation.